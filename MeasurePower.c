#include "simpletools.h"                      // Include simpletools
#include "adcDCpropab.h"                      // Include adcDCpropab
#include "math.h"

int main()                                    // main function
{
  adc_init(21, 20, 19, 18);                   // CS=21, SCL=20, DO=19, DI=18

  float v2;                                   // Voltage variable ADC pin 2
  int R = 1978;

  while(1)                                    // Loop repeats indefinitely
  {
    v2 = adc_volts(2);                        // Check A/D 2                
    float P = (1000*v2*v2)/R;                 //mW
    
    putChar(HOME);                            // Cursor -> top-left "home"
    print("A/D2 = %f V%c\n", v2, CLREOL);     // Display volts
    print("P = %f mW%c\n",P,CLREOL);
    pause(100);                               // Wait 1/10 s
  }  
}
