/*
Horizontal Servo Movement
http://learn.parallax.com/propeller-c-tutorials
*/
#include "simpletools.h" // Include simple tools
#include "servo.h"
#include "math.h"

volatile float Horiz;
void Horizontal();


int main()
{
int hangle=900; //Horizontal Angle set to 90 degrees
servo_angle(16,hangle);//declare servo as pin 16 and moves it to the set degree
cog_run(Horizontal,512); //starts the cog
while(1)
{
if(Horiz>1 && hangle<=1750) //spins clockwise when Horiz>1 and less than 175 degrees
{
hangle=hangle+50; //changes 5 degrees every turn
servo_angle(16,hangle);
}
if(Horiz<-1 && hangle>=50) //spins counterclockwise
{
hangle=hangle-50; //moves 5 degrees counterclowise
servo_angle(16,hangle);
}
pause(100); //pause for system to get the incoming data
}
}

void Horizontal()
{
Horiz=-10; // starts rotating counterclockwise from start the of 90 degrees
while(1)
{
Horiz++; // once Horiz becomes >1 it switches directions and goes counterclockwise
pause(100);
}
}
