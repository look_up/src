import os
import time 
from time import sleep
from datetime import datetime
import serial
import smtplib
import time

serialport = serial.Serial('/dev/ttyUSB0', 115200, timeout = None)
file = open("/media/pi/ALEKS/SolarData/solardata.txt","w")
file.write("Date,Time,Temp F\n")
print('Logging Data')
print('Date,Time,Temp F\n')

try:
	while True:
		line = serialport.readline()
		now = datetime.now()
		date_time = now.strftime("%m/%d/%y,%H:%M:%S")
        	file.write("%s," %(date_time))
		file.write("%s" %(line))
		print('%s' %(line))
		sleep(0.1)

except KeyboardInterrupt:
	print('Data Logging Ended')
file.close()
