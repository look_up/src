var http = require('http').createServer(handler); //require http server, and cr$
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the htt$

http.listen(8080); //listen to port 8080

function handler (req, res) { //create server
  fs.readFile(__dirname + '/index.html', function(err, data) { //read file inde$
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    }
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}
io.on('connection', function (socket) { // WebSocket Connection
setInterval(function(){      // setInterval is used to repeat the action
   // read text file by line and turn into an array
   var array = fs.readFileSync('report.txt').toString().split("\n");
   var volt = (array[0]);    // Assign first element as volt
   var power = (array[1]);   // Assign second element as power
   var temp = (array[2]);    // Assign third element as temp
   socket.emit('volt',{'volt': volt});          // send volt to websocket server
   socket.emit('power',{'power': power});       // send power to websocket serv$
   socket.emit('temp',{'temp': temp});          // send temp to websocket server
}, 500);        // interval of repeat : 0.5 second
});