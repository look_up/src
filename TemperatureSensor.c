#include "simpletools.h"                      // Include simpletools
#include "math.h"

int main()                                    // main function
{
  double t, tau, R, A, B, C, logR, logR3, Temp, TempK;
  A = 0.0055;                                 //steinhart-hart constants
  B = -0.00051482;
  C = 0.0000033748;
  
  while(1)                                    // Endless loop
  {
    high(7);                                  // Set P5 high
    pause(500);                                 // Wait for circuit to charge
    t = rc_time(7, 1);                    // Measure decay time on P5
    tau = 0.000001*(t/0.693);
    R = tau/0.0000117;
    logR = log(R);
    logR3 = pow(logR,3);
    TempK = 1/(A+(B*logR)+(C*logR3));     //steinhart-hart equation
    Temp = (TempK-273.15)*(1.8)+32;       //conversion to fahrenheit
    
    //putChar(HOME);
    //print("R = %f Temp = %f F%c\n",R,Temp,CLREOL);
    printf("%f\n",Temp); //send temp to RPi
    // Display temp and resistance
  }    
}