/*
Dual Axis Solar Tracker
http://learn.parallax.com/propeller-c-tutorials
*/
#include "simpletools.h" // Include simple tools
#include "servo.h"
#include "math.h"

volatile float Vert, Horiz; //global variables to pass into the main function
void light();


int main() // Main function
{
// Add startup code here.
int vangle=900; //set vertical servo to 90 degrees
int hangle=900; //set horizontal servo to 90 degrees 
servo_angle(14,vangle);
servo_angle(16,hangle);
cog_run(light,512); //start the cog program
while(1)
{
if(Vert>1 && vangle<=1750) //if the value of Vert is greater than 1 from the LDR circuit and less than 175 degrees turn clockwise
{
vangle=vangle+50; //move in 5 degree increments clockwise
servo_angle(14,vangle);
}
if(Vert<-1 && vangle>=50) //if the value of Vert is less than -1 from the LDR circuit and greater than 5 degrees turn counterclockwise
{
vangle=vangle-50;  //move in 5 degree increments counterclockwise
servo_angle(14,vangle);
}
pause(100);
if(Horiz>1 && hangle<=1750) //if the value of Horiz is greater than 1 from the LDR circuit and less than 175 degrees turn clockwise
{
hangle=hangle+50; //move in 5 degree increments clockwise
servo_angle(16,hangle);
}
if(Horiz<-1 && hangle>=50) //if the value of Horiz is less than -1 from the LDR circuit and greater than 5 degrees turn counterclockwise
{
hangle=hangle-50; //move in 5 degree increments counterclockwise
servo_angle(16,hangle);
}
pause(100);
}
}

void light()
{
float TR, TL, BR, BL, t1, t2, t3, t4;
while(1)
{
high(0); // Set P0
pause(1); // Wait for circuit to charge
t3 = rc_time(0, 1); // Measure decay time on P0
high(1); // Set P1
pause(1);
t2 = rc_time(1, 1); // Measure decay time on P1
high(12); // Set P0
pause(1); // Wait for circuit to charge
t4 = rc_time(12, 1); // Measure decay time on P0
high(9); // Set P1
pause(1);
t1 = rc_time(9, 1); // Measure decay time on P1
TR = t1/12; // light level integer
TL = t2/10;
BL = t3/10;
BR = t4/11.5;
float Tavg, Bavg, Lavg, Ravg;
Tavg = (TR+TL)/2; //the average of the top right and left photoresistors
Bavg = (BL+BR)/2; //average of the bottom left and bottom right photoresistors
Lavg = (BL+TL)/2; //average of the bottom left and top left photoresistors
Ravg = (BR+TR)/2; //average of the bottom right and top right photoresistors
Vert = Tavg-Bavg; //tells vertical servo which way to turn
Horiz = Lavg-Ravg; //tells horizontal servo which way to turn.
putChar(HOME); // Cursor -> top-left "home"
print("TR = %f, TL = %f, BL = %f, BR = %f%c\n",TR,TL,BL,BR,CLREOL);
print("Vert = %f, Horiz = %f%c\n",Vert, Horiz, CLREOL);
pause(50);
}
}
