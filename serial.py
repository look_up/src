import serial # include serial library
import time   # include time library

# Read serial port through port ttyUSB0
serialport = serial.Serial('/dev/ttyUSB0', 115200, timeout = 1)

while True:     # infinite loop
   line = serialport.readline() # read serial port by line
   result = line.find(";")   # Check if there is semicolon
   if result > 0:
        str = line.split(";")   # At the ";" split the line into an array
        volt=str[0]             # Assign variable for for first element
        power=str[1]            # Assign variable for for second element
        temp=str[2]             # Assign variable for for third element
        file = open("report.txt","w") # open or create text file
        file.write("%s\n" %(volt))    # store first value into text file
        file.write("%s\n" %(power))    # store second value into text file
        file.write("%s\n" %(temp))    # store third value into text file
        file.close()                  # close the file