#include "simpletools.h"                      // Include simple tools
#include "servo.h"
#include "math.h"
#include "adcDCpropab.h"

void light();
void temp_power();
void servo();
volatile float Vert, Horiz, v2, P;
volatile double Temp;

int main()                                    // Main function
{
  cog_run(temp_power,256);
  cog_run(servo, 128);
  cog_run(light,512);
  while(1)
  {
    printf("%f;%f;%f\n",v2,P,Temp);
    pause(500);
  }    
}

void servo()
{
  int vangle=900;
  int hangle=900;
  int angle = 40; 
  int rest = 150;
  servo_angle(14,vangle);
  servo_angle(16,hangle);
  
  while(1)
  {
    if(Vert>1 && vangle<=1700) 
    {
      vangle=vangle+angle;
      servo_angle(14,vangle);
    }      
    if(Vert<-1 && vangle>=100)
    {
      vangle=vangle-angle;
      servo_angle(14,vangle);
    }   
    pause(rest);
    if(vangle>=900)
    {    
      if(Horiz>1 && hangle<=1750) 
      {
        hangle=hangle+angle;
        servo_angle(16,hangle);
      }      
      if(Horiz<-1 && hangle>=50)
      {
        hangle=hangle-angle;
        servo_angle(16,hangle);
      }
    }
    if(vangle<900)
    {
      if(Horiz<-1 && hangle<=1750) 
      {
        hangle=hangle+angle;
        servo_angle(16,hangle);
      }      
      if(Horiz>1 && hangle>=50)
      {
        hangle=hangle-angle;
        servo_angle(16,hangle);
      }
    }                      
    pause(rest);    
  }  
}  

void light() 
{  
  float TR, TL, BR, BL, t1, t2, t3, t4;
  float Tavg, Bavg, Lavg, Ravg;
  while(1)
    {
    high(0); // Set P0
    pause(1); // Wait for circuit to charge
    t3 = rc_time(0, 1); // Measure decay time on P0
    high(1); // Set P1
    pause(1);
    t2 = rc_time(1, 1); // Measure decay time on P1
    high(12); // Set P0
    pause(1); // Wait for circuit to charge
    t4 = rc_time(12, 1); // Measure decay time on P0
    high(9); // Set P1
    pause(1);
    t1 = rc_time(9, 1); // Measure decay time on P1
    TR = t1/12; // light level integer
    TL = t2/11;
    BL = t3/10;
    BR = t4/11.5;
    Tavg = (TR+TL)/2;
    Bavg = (BL+BR)/2;
    Lavg = (BL+TL)/2;
    Ravg = (BR+TR)/2;
    Vert = Tavg-Bavg; //tells vertical servo which way to turn
    Horiz = Lavg-Ravg; //tells horizontal servo which way to turn.
    //putChar(HOME); // Cursor -> top-left "home"
    //print("TR = %f, TL = %f, BL = %f, BR = %f%c\n",TR,TL,BL,BR,CLREOL);
    //print("Vert = %f, Horiz = %f%c\n",Vert, Horiz, CLREOL);
    pause(50);
    }
}

void temp_power()
{
  double t, tau, R, A, B, C, logR, logR3, TempK;
  A = 0.0055; //steinhart-hart constants
  B = -0.00051482;
  C = 0.0000033748;
  adc_init(21, 20, 19, 18); // CS=21, SCL=20, DO=19, DI=18

  int Rpanel = 1978;//resistance in parallel with solar panel
  while(1) // Endless loop  
  {
    high(7); // Set P5 high
    pause(500); // Wait for circuit to charge
    t = rc_time(7, 1); // Measure decay time on P5
    tau = 0.000001*(t/0.693);
    R = tau/0.0000117;
    logR = log(R);
    logR3 = pow(logR,3);
    TempK = 1/(A+(B*logR)+(C*logR3)); //steinhart-hart equation
    Temp = (TempK-273.15)*(1.8)+32; //conversion to fahrenheit
    v2 = adc_volts(2); // Check A/D 2
    P = (1000*v2*v2)/Rpanel; //mW
    //print("R = %f Temp = %f F%c\n",R,Temp,CLREOL);
    //printf("%f\n",Temp);
    //print("%f F,%f V,%f mW\n", Temp, v2, P); // Display volts 
    //print("%f\n", Temp);
    //Display temp and resistance
    pause(100); // Wait 1/10 s
  }
} 
