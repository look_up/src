#include "simpletools.h"                      // Include simple tools
#include "math.h"

int main()                                    // Main function
{
  float TR, TL, BR, BL, t1, t2, t3, t4;
 
  while(1)
  {
    high(0);                                 // Set P0                              
    pause(1);                                // Wait for circuit to charge
    t3 = rc_time(0, 1);                      // Measure decay time on P0
    high(1);                                 // Set P1
    pause(1);
    t2 = rc_time(1, 1);                      // Measure decay time on P1
    high(12);                                // Set P12                              
    pause(1);                                // Wait for circuit to charge
    t4 = rc_time(12, 1);                     // Measure decay time on P12
    high(9);                                 // Set P9
    pause(1);
    t1 = rc_time(9, 1);                      // Measure decay time on P9                     
    TR = t1/12;                              // light level calibration
    TL = t2/10; 
    BL = t3/10;
    BR = t4/11.5;
    
    float Tavg, Bavg, Lavg, Ravg, Vert, Horiz;
    Tavg = (TR+TL)/2;
    Bavg = (BL+BR)/2;
    Lavg = (BL+TL)/2;
    Ravg = (BR+TR)/2;
    Vert = Tavg-Bavg; //tells vertical servo which way to turn
    Horiz = Lavg-Ravg; //tells horizontal servo which way to turn.
    
    putChar(HOME);                           // Cursor -> top-left "home"
    print("TR = %f, TL = %f, BL = %f, BR = %f%c\n",TR,TL,BL,BR,CLREOL);
    print("Vert = %f, Horiz = %f%c\n",Vert, Horiz, CLREOL); //print values        
    pause(1000);
  }  
}
